const Parse = require('parse/node')

class Score extends Parse.Object {
    constructor() {
        super('Score');
    }
}

class Bucket extends Parse.Object {
    constructor() {
        super('Bucket');
    }
}

class HighScore extends Parse.Object {
    constructor() {
        super('HighScore');
    }
}

Parse.Cloud.beforeSave('Score', function(request, response) {
    let score = request.object;
    let _score = score.get('score');
    let query = new Parse.Query(Bucket);
    query.lessThanOrEqualTo('minValue', _score);
    query.greaterThan('maxValue', _score);
    query.first().then(function(object) {
        if (!object) {
            let query2 = new Parse.Query(Bucket)
            query2.descending('maxValue');
            return query2.first().then(function (lastBucket) {
                if (lastBucket) {
                    lastBucket.set('maxValue', _score + 1000);
                    lastBucket.save();
                    return lastBucket;
                }
                else {
                    let firstBucket = new Bucket();
                    firstBucket.set('maxValue', 10000);
                    firstBucket.set('minValue', 0);
                    firstBucket.set('count', 0);
                    firstBucket.save();
                    return firstBucket;
                }
            });
        }
        else {
            return object;
        }
    }).then(function(object) {
        if (object) {
            let prevBucket = score.get('bucket');
            let balBucket = balanceBucketSize(object, _score);
            if (prevBucket && prevBucket !== balBucket) {
                prevBucket.increment('count', -1);
                prevBucket.save();
            }
            if (prevBucket !== balBucket) {
                balBucket.increment('count', 1);
                return balBucket.save();
            }
        }
    }).then( (bucket) => {
        if (bucket) {
            score.set('bucket', bucket);
            response.success(score);
        }
    });
});

function balanceBucketSize(sBucket, score) {
    if (sBucket && sBucket.get('count') > 1000) {
        let border = (sBucket.get('minValue') + sBucket.get('maxValue')) / 2;
        if (border - sBucket.get('minValue') < 50) {
            return sBucket;
        }
        let newBucket = new Bucket();
        newBucket.set('minValue', border);
        newBucket.set('maxValue', sBucket.get('maxValue'));
        newBucket.set('count', 0);
        newBucket.save();
        sBucket.set('maxValue', border);
        sBucket.save();
        let query = new Parse.Query(Score);
        query.equalTo('bucket', sBucket);
        query.greaterThanOrEqualTo('score', border);
        query.find({
            success: function (results) {
                for (let result of results) {
                    result.save();
                }
            }
        });
        if (score >= border) {
            return newBucket;
        }
    }
    return sBucket;
}


Parse.Cloud.afterSave('Score', function(request, response) {
    let score = request.object;
    score.get('user').fetch().then( (user) => {
        return user.get('city');
    }).then((domain) => {
        isHighScore(score, domain);
        isHighScore(score, 'global');
    });
    response.success();
});

function isHighScore(score, domain) {
    let query = new Parse.Query(HighScore);
    query.equalTo('domain', domain);
    let innerQuery = new Parse.Query(Score);
    innerQuery.greaterThanOrEqualTo('score', score.get('score'));
    innerQuery.notEqualTo('user', score.get('user'));
    query.matchesQuery('record', innerQuery);
    query.count().then((count) => {
        if (count < 20) {
            let highScore = new HighScore();
            highScore.set('domain', domain);
            highScore.set('record', score);
            highScore.set('ranking', count + 1);
            rerank(highScore);
            highScore.save();
            return true;
        }
        else {
            return false;
        }
    });
}



function rerank(highScore){
    let query = new Parse.Query(HighScore);
    query.equalTo('domain', highScore.get('domain'));
    query.equalTo('record', highScore.get('record'));
    return query.first().then( (prevHighScore) => {
        let updateQuery = new Parse.Query(HighScore);
        updateQuery.equalTo('domain', highScore.get('domain'))
        updateQuery.greaterThanOrEqualTo('ranking', highScore.get('ranking'));
        if (prevHighScore) {
            updateQuery.lessThan('ranking', prevHighScore.get('ranking'));
        }
        updateQuery.notEqualTo('record', highScore.get('record'));
        return updateQuery.find().then( (results) => {
            for (let result of results) {
                if (result.get('ranking') == 20) {
                    result.destroy();
                }
                else {
                    result.increment('ranking', 1);
                    result.save();
                }
            }
        }).then(
            () => prevHighScore.destroy()
        );
    });
}


Parse.Cloud.define('getUserRank', function (request, response) {
    let objQuery = new Parse.Query(Score);
    let domain = request.params.domain;
    let userScore;
    objQuery.get(request.params.userScore).then((score) => {
        userScore = score;
        if (domain == 'global') {
            let query1 = new Parse.Query('Bucket');
            query1.greaterThan('minValue', userScore.get('score'));
            query1.select("count");
            let query2 = new Parse.Query('Score');
            query2.equalTo('bucket', userScore.get('bucket'));
            query2.greaterThanOrEqualTo('score', userScore.get('score'));
            query2.lessThanOrEqualTo('updatedAt', userScore.updatedAt);
            Promise.all([query1.find().then((results) => {
                let sum = 0;
                for (let bucket of results) {
                    sum += bucket.get('count');
                }
                return sum;
            }), query2.count()]).then( (values) => {
                response.success(values[0] + values[1]);
            });
        }
        else {
            let query = new Parse.Query(Score);
            let innerQuery = new Parse.Query(Parse.User);
            innerQuery.equalTo('city', domain);
            query.matchesQuery('user', innerQuery);
            query.greaterThanOrEqualTo('score', userScore.get('score'));
            query.lessThanOrEqualTo('updatedAt', userScore.updatedAt);
            query.count().then( (count) => {
                response.success(count);
            });
        }
    });

});


Parse.Cloud.define('addPoints', function (request, response) {
    var Score = Parse.Object.extend("Score");
    var points = request.params.state == 'winner'? 150 : 50;
    var user = request.user;
    var query = new Parse.Query(Score);
    query.equalTo("user", user);
    query.first().then(function(object) {
        if (object) {
            object.increment('score', points);
            object.save();
        }
        else {
            let score = new Score();
            score.set('score', points);
            score.set('user', user);
            score.save();
        }
    });
    response.success();
});
