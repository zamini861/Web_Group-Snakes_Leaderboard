const express = require('express');
const ParseServer = require('parse-server').ParseServer;
const Parse = require('parse/node');
const app = express();

var api = new ParseServer({
    databaseURI: 'mongodb://localhost:27017/dev',
    cloud: 'cloud/main.js',
    appId: 'myAppId123456',
    masterKey: '24',
    serverURL: 'http://localhost:1337/parse',
});

app.use('/parse', api)

app.listen(3000, () => console.log('Example app listening on port 3000!'))

var httpServer = require('http').createServer(app);
httpServer.listen(1337, function() {
    console.log('parse-server-example running on port 1337.');
});

// This will enable the Live Query real-time server
ParseServer.createLiveQueryServer(httpServer);
