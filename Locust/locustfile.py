from locust import HttpLocust, TaskSet, task
from json import dumps


class UserBehavior(TaskSet):
    def on_start(self):
        self.common_params = {
            '_ApplicationId': 'myAppId123456',
            '_ClientVersion': 'js1.11.0',
            '_InstallationId': '66a523b6-6572-0935-dccb-b2a21cd9551e',
            '_method': 'GET'
        }
        #self.signup()
        self.login()

    def signup(self):
        self.client.post("/users", dict({
            "username": "Jack_34",
            "password": "1236",
            "city": "London"
        }, **self.common_params))

    def login(self):
        self.client.post("/login", dict({
            "username": "Jack_34",
            "password": "1236",
        }, **self.common_params))

    @task(1)
    def get_score(self):
        self.client.post("/classes/Score", dict({
            'limit': 1,
            'where': dumps({
                'user': {
                    '__type': 'Pointer',
                    'className': '_User',
                    'objectId': 'Ot7VO6TGX7'
                }
            }),
        }, **self.common_params))

    @task(2)
    def get_global_high_scores(self):
        self.get_high_scores('global')
        self.get_user_rank('global')

    @task(3)
    def get_city_high_scores(self):
        self.get_high_scores('London')
        self.get_user_rank('London')

    @task(4)
    def add_game_win_score(self):
        self.add_game_score('winner')

    @task(5)
    def add_game_lost_score(self):
        self.add_game_score('loser')

    def add_game_score(self, status):
        self.client.post("/functions/addPoints", data={
            'state': status,
        }, json=self.common_params,
            headers={
            'X-Parse-Application-Id': 'myAppId123456',
            'X-Parse-REST-API-Key': '24',
        })

    def get_high_scores(self, domain):
        self.client.post("/classes/HighScore", dict({
            'include': 'record, record.user',
            'order': 'ranking',
            'where': dumps({
                'domain': domain
            }),
        }, **self.common_params))

    def get_user_rank(self, domain):
        self.client.post("/functions/getUserRank", data={
            'domain': domain,
            'userScore': 'bIqEQ3MDJR',
        }, json=self.common_params,
            headers={
            'X-Parse-Application-Id': 'myAppId123456',
            'X-Parse-REST-API-Key': '24',
        })


class WebsiteUser(HttpLocust):
    task_set = UserBehavior
    min_wait = 50
    max_wait = 200
