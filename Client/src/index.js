import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import './style.css';
const Parse = require('parse/node')


Parse.initialize('myAppId123456');
Parse.serverURL = 'http://localhost:1337/parse';


class RecordRow extends Component {

    render() {
        return (
            <tr className={this.props.class}>
                <td>
                    {this.props.record.get('ranking')}
                </td>
                <td>
                    {this.props.record.get('record').get('user').get('username').toString()}
                </td>
                <td>
                    {this.props.record.get('record').get('user').get('city').toString()}
                </td>
                <td>
                    {this.props.record.get('record').get('score').toString()}
                </td>
            </tr>
        );
    }
}

class RecordTable extends Component {

    render() {
        const records = this.props.records;
        const userRank = this.props.userRank;
        const highScoreRows = records.map((record) => (
            <RecordRow class={userRank && userRank.get('record').id === record.get('record').id? "user" : ""}
                       record={record} key={record.get('ranking').toString()}/>
        ));
        const userRow = <RecordRow class={"user"} record={this.props.userRank} key={(records.length+1).toString()}/>
        let tableRows;
        if (this.props.userRank && this.props.userRank.get('ranking') > records.length) {
            tableRows = [...highScoreRows, userRow];
        }
        else {
            tableRows = highScoreRows;
        }
        return (
            <table>
                <thead>
                    <tr>
                        <th> Rank </th>
                        <th> Username </th>
                        <th> City </th>
                        <th> Score </th>
                    </tr>
                </thead>
                <tbody>
                    {tableRows}
                </tbody>
            </table>
        );
    }
}



class Button extends Component {

    render() {
        return (
            <span>
                <button type="button" className={this.props.class} onClick={this.props.onClick}>{this.props.title}</button>
            </span>
        );
    }
}



class LeaderBoard extends Component {

    constructor(props) {
        super(props);
        Parse.User.logIn("Jack_34", "1236").then(() => {
            let query = new Parse.Query('Score');
            query.equalTo('user', Parse.User.current());
            query.first().then((result) => {
                if (result) {
                    this.state = {
                        records: [],
                        userScore: result,
                    };
                }
            }).then(() => {
                this.globalList();
            });
        });
        this.globalList = this.globalList.bind(this);
        this.localList = this.localList.bind(this);
    }

    fetchRecords(domain) {
        let query = new Parse.Query('HighScore');
        query.equalTo('domain', domain);
        query.ascending('ranking');
        query.include("record");
        query.include("record.user");
        return query.find();
    }

    fetchUserRank(domain) {
        return Parse.Cloud.run('getUserRank', {
            userScore: this.state.userScore.id,
            domain: domain,
        }).then( (rank) => {
            let userRank = new Parse.Object('HighScore');
            userRank.set('domain', domain);
            userRank.set('record', this.state.userScore);
            userRank.set('ranking', rank);
            return userRank;
        });
    }

    globalList() {
        this.fetchRecords('global').then((records) => {
            this.setState({
                ...this.state,
                records: records
            });
        });
        this.fetchUserRank('global').then((rank) => {
            this.setState({
                ...this.state,
                userRank: rank,
            });
        });
    }

    localList() {
        this.fetchRecords(Parse.User.current().get('city')).then((records) => {
            this.setState({
                ...this.state,
                records: records,
            });
        });
        this.fetchUserRank(Parse.User.current().get('city')).then((rank) => {
            this.setState({
                ...this.state,
                userRank: rank,
            });
        });
    }


    render() {
        return (
            <div>
                <h1> Leaderboard </h1>
                <Button class={"round-left"} title={"Global"} onClick={this.globalList}/>
                <Button class={"round-right"} title={"City"} onClick={this.localList}/>
                {this.state && <RecordTable records={this.state.records} userRank={this.state.userRank}/>}
            </div>
        );
    }
}

export default LeaderBoard;


ReactDOM.render(
    <LeaderBoard/>,
    document.getElementById('root')
);
